---
layout: post
title:  "Load Balancing in Wireless Mesh Network"
date:   2016-12-19 15:32:16 -0300
categories: project
---

Programs Needed:

[VirtualBox][virtualbox]

[Mininet][mininet] + Wireshark

[Xming][xming]

[Putty][putty]


[virtualbox]: https://www.virtualbox.org/
[mininet]: http://mininet.org/
[xming]: https://sourceforge.net/projects/xming/
[putty]: http://www.putty.org/
---
Instructions:

1.	We start of by opening Miniedit located in mininet.

		sudo mininet/examples/miniedit.py


2.	Create a topology like one below

	![alt text](/loadbalancer/images/project/image1.jpeg "image")

3.	Enable **CLI**.

	Goto **Edit>Preferences>Check Start CLI**

4.	Click **Run**. (Note: You need to type exit in the terminal first before you press the stop button)

5.	First, we will simulate the traditional routers. Then we will ping h3 from h1 to verify the connection.

	a.	For the traditional router, it always chooses the shortest path possible that is from h1 to switch s2, to switch s3, then finally to h3.


	![alt text](/loadbalancer/images/project/image2.jpeg "image")
                                                                            
	b.	To simulate the connections, we add the following commands in another putty terminal:

		sudo ovs-ofctl add-flow s2 in_port=1,actions=output:3
		sudo ovs-ofctl add-flow s3 in_port=3,actions=output:1

	c.	We can verify the connection by running xterm in miniedit terminal

		xterm h1 h2 h3

	In the h3 window, 

		add the command: tcpdump -XX -n -i h3-eth0

	d.	On h1 execute

		ping 10.0.0.3 

	(this is the address of h3). You should see the following on h3. 


	![alt text](/loadbalancer/images/project/image3.jpeg "image")
 
	This means h3 received the packets from h1.

	e.	We can monitor the load between s2 and s3 using wireshark. Start wireshark executing:

		sudo wireshark &

	Goto **Capture> Interfaces>Check s2-eth3**. Click **Start**.
	
	Since we are still pinging h3. Wireshark should capture packets.


	![alt text](/loadbalancer/images/project/image4.jpeg "image")

	f.	Graph the packets. Go to **statistics>IO Graphs**. 

	It should register some atleast 50 bytes per tick.


	![alt text](/loadbalancer/images/project/image5.jpeg "image")

6.	Next we will simulate the load in s2-eth3. We do this by simply pinging h3 from h2. To do that we add the following flows:

		sudo ovs-ofctl add-flow s2 in_port=2,actions=output:3

	On h2 terminal:

		ping 10.0.0.3

	The wireshark should notice an increase in packets per tick

	![alt text](/loadbalancer/images/project/image6.jpeg "image")

	This is the load that we want to load balance

7.	To apply load balancing, we simply change the flows in switch. This is easier said than done. Note this part can be done using a controller script such as in POX. See tutorial in creating switches. What we are about to show here is the basic operation in a Software Defined Network. That is the simple way of changing the flows between switches.

	a.	We first delete the flows in switch s2: 

		sudo ovs-ofctl del-flows s2

	b.	Then we add the following command (Note: This can be done in a script)

		sudo ovs-ofctl add-flow s2 in_port=1,actions=output:3
		sudo ovs-ofctl add-flow s2 in_port=2,actions=output:4
		sudo ovs-ofctl add-flow s1 in_port=1,actions=output:2
		sudo ovs-ofctl add-flow s3 in_port=4,actions=output:1

	c.	The commands above migrate the flows coming from h2 around the mesh. This will decrease the load in s2-eth3 effectively balancing the whole network. Wireshark should verify this.

	![alt text](/loadbalancer/images/project/image7.jpeg "image")

	d.	Also capturing packets on s2-eth4 shows that load balancing is working. 

	![alt text](/loadbalancer/images/project/image8.jpeg "image")

8.	Since we are also working on wireless devices, we can also use this technique to route traffic from a busy channel to another access point. The method is same as the above and can also be implemented using a controller.

	a.	First we simulate a busy/lossy channel, we do this by first typing exit in CPI terminal then stoping the miniedit.

	b.	Right click on the s2-eth3 link and choose properties.  Here you can add Badwidth limit, delay, loss, queue size, jitter and speedup. In this example, we will simulate 50% loss in packet transmission

	![alt text](/loadbalancer/images/project/image9.jpeg "image")

	c.	Click **OK**. Then **Run** again.

	d.	Since we stopped the simulation from above. We will need to add the necessary flows again.

	On CPI terminal,

		xterm h1 h3

	On another terminal,

		sudo ovs-ofctl add-flow s2 in_port=1,actions=output:3
		sudo ovs-ofctl add-flow s3 in_port=3,actions=output:1

	On h1 terminal,

		ping 10.0.0.3

	e.	Start wireshark again


		sudo wireshark &

	Capture **s2-eth3**. Then graph it in IO graph. Notice the difference from above. This is due to the 50% loss transmission which is common in wireless networks.
	
    ![alt text](/loadbalancer/images/project/image10.jpeg "image")
    
	f.	We simply find an alternate path for the transmission,

		sudo ovs-ofctl del-flows s2
		sudo ovs-ofctl add-flow s2 in_port=1,actions=output:4
		sudo ovs-ofctl add-flow s1 in_port=1,actions=output:2
		sudo ovs-ofctl add-flow s3 in_port=4,actions=output:1

	g.	Wireshark should stop capturing packets **s2-eth3**. While capturing **s2-eth4** should yield a smooth curve.


	![alt text](/loadbalancer/images/project/image11.jpeg "image")


